import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const Login = ({ onSignIn }) => {
  const signIn = async () => {
    // Simulate a user sign-in process
    const dummyUserInfo = {
      id: 'dummyUser',
      name: 'Dummy User',
      email: 'dummy@example.com',
    };
    
    // Call the onSignIn prop with the dummy user information
    onSignIn(dummyUserInfo);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome to the App</Text>
      <TouchableOpacity onPress={signIn} style={styles.signInButton}>
        <Text style={styles.signInButtonText}>Sign In</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0D1117',
  },
  title: {
    fontSize: 24,
    color: '#C9D1D9',
    marginBottom: 20,
  },
  signInButton: {
    backgroundColor: '#58A6FF', // A vibrant blue for the button
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  signInButtonText: {
    color: '#FFFFFF', // White text for the button
    fontSize: 18,
  },
});

export default Login;
