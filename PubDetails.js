import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

const PubDetails = ({ route, navigation }) => {
  const { pub } = route.params;

  // Dummy function for Follow action
  const handleFollow = () => {
    alert('Followed the pub successfully!');
  };

  // Dummy function to open the Menu
  const openMenu = () => {
    alert('Menu opened!');
  };

  // Dummy function to view Discounts
  const viewDiscounts = () => {
    navigation.navigate('PubDiscounts', { discounts: pub.discounts });
  };

  return (
    <ScrollView style={styles.container}>
      <Image source={{ uri: pub.image }} style={styles.image} />
      <Text style={styles.title}>{pub.name}</Text>
      <Text style={styles.info}>Town: {pub.town}</Text>
      <Text style={styles.info}>Country: {pub.country}</Text>
      <Text style={styles.description}>{pub.description}</Text>
      <TouchableOpacity style={styles.button} onPress={handleFollow}>
        <Text style={styles.buttonText}>Follow Pub</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={openMenu}>
        <Text style={styles.buttonText}>View Menu</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={viewDiscounts}>
        <Text style={styles.buttonText}>View Discounts</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0D1117',
  },
  image: {
    width: '100%',
    height: 300,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#58A6FF',
    padding: 10,
  },
  info: {
    fontSize: 18,
    color: '#C9D1D9',
    paddingLeft: 10,
  },
  description: {
    fontSize: 16,
    color: '#C9D1D9',
    padding: 10,
  },
  button: {
    marginHorizontal: 10,
    marginTop: 15,
    backgroundColor: '#58A6FF',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontWeight: 'bold',
  },
  // Add more styles as needed
});

export default PubDetails;
