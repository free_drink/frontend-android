import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PubsStackNavigator from './PubsStackNavigator'; // Assuming this is correctly set up
import PubsList from './PubsList';

// Import other screens here

const Tab = createBottomTabNavigator();

function AppNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Pubs') {
              iconName = focused ? 'local-drink' : 'local-drink';
            } // Add more icons for other tabs
            if (route.name === 'Pubs2') {
              iconName = focused ? 'local-drink' : 'local-drink';
            }
            if (route.name === 'Pubs3') {
              iconName = focused ? 'local-drink' : 'local-drink';
            }

            // You can return any component that you like here!
            return <MaterialIcons name={iconName} size={size} color={color} />;
          },
        })}
      >
        <Tab.Screen name="Pubs" component={PubsStackNavigator} />
        <Tab.Screen name="Pubs2" component={PubsList} />
        <Tab.Screen name="Pubs3" component={PubsList} />

        {/* Add other tabs here */}
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;
