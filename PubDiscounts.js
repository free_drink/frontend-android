import React, { useState, useEffect } from 'react';
import { Modal, ScrollView, View, Text, TouchableOpacity, Alert, Platform, Linking, StyleSheet } from 'react-native';
import * as Sharing from 'expo-sharing';
import * as FileSystem from 'expo-file-system';

// Dummy function for making an HTTP POST request
async function sendDiscountToBackend(discount) {
    try {
        const response = await fetch('YOUR_BACKEND_ENDPOINT', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(discount),
        });
        if (!response.ok) {
            throw new Error('Network response was not ok.');
        }
        const data = await response.json();
        Alert.alert("Success", "Discount successfully claimed!");
    } catch (error) {
        Alert.alert("Error", "Failed to claim discount.");
    }
}

const PubDiscounts = ({ route }) => {
    const { discounts } = route.params;
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedDiscount, setSelectedDiscount] = useState({});

    const shareToInstagramStory = async () => {
      // bar imageUrl should be bar specific and fetch from the backend
      const imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Alain_R%C3%A9gnier%2C_pr%C3%A9fer%2C_d%C3%A9l%C3%A9gu%C3%A9_interminist%C3%A9riel_pour_l%27h%C3%A9bergement_et_l%27acc%C3%A8s_au_logement_des_personnes_sans-abri_ou_mal_log%C3%A9es.JPG/1200px-Alain_R%C3%A9gnier%2C_pr%C3%A9fer%2C_d%C3%A9l%C3%A9gu%C3%A9_interminist%C3%A9riel_pour_l%27h%C3%A9bergement_et_l%27acc%C3%A8s_au_logement_des_personnes_sans-abri_ou_mal_log%C3%A9es.JPG'; // URL of the image
      const filename = FileSystem.cacheDirectory + 'downloaded-image.png';
   
      // Download the image from the URL to the local file system
      const { uri } = await FileSystem.downloadAsync(imageUrl, filename);
   
      // Check if sharing is available
      if (await Sharing.isAvailableAsync()) {
        try {
          await Sharing.shareAsync(uri, {
            mimeType: 'image/jpeg', // Make sure to use the correct MIME type
            dialogTitle: 'Share to Instagram Stories',
            UTI: 'public.jpeg' // This is specific to iOS
          });
        } catch (error) {
          console.error('Error while sharing:', error);
        }
      } else {
        Alert.alert('Sharing not available', 'Unfortunately, sharing is not available on your device.');
      }
    };

    const redeemDiscount = async () => {
        if (selectedDiscount.type === 'instagram') {
            shareToInstagramStory(selectedDiscount.imageUrl);
        } else {
            // Handle other discount types or direct backend redemption
            await sendDiscountToBackend(selectedDiscount);
        }
        setModalVisible(false);
    };

    const openDiscountModal = (discount) => {
        setSelectedDiscount(discount);
        setModalVisible(true);
    };

    return (
      <ScrollView style={styles.container}>
        <Text style={styles.header}>Pub Discounts</Text>
        {discounts.map((discount, index) => (
          <TouchableOpacity key={index} onPress={() => openDiscountModal(discount)} style={styles.discountContainer}>
            <Text style={styles.discountTitle}>{discount.title}</Text>
            <Text style={styles.discountDescription}>{discount.description}</Text>
          </TouchableOpacity>
        ))}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => setModalVisible(!modalVisible)}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text>How to Redeem:</Text>
              <Text style={styles.modalInstruction}>{selectedDiscount.redeemInstructions || 'Follow the instructions provided at the venue or mention this offer.'}</Text>
              <TouchableOpacity
                onPress={redeemDiscount}
                style={[styles.button, styles.buttonClose]}
              >
                <Text style={styles.textStyle}>Redeem</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </ScrollView>
    );}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#0D1117', // Dark background for the entire view
    },
    header: {
      fontSize: 24,
      fontWeight: 'bold',
      color: '#58A6FF', // Highlight color for the header
      padding: 10, // Padding around the header for spacing
      borderBottomWidth: 1, // Subtle separator
      borderBottomColor: '#30363d', // Slightly lighter color for separation
    },
    discountContainer: {
      backgroundColor: '#161B22', // Slightly lighter background for each discount item
      padding: 20, // Padding inside each discount container for spacing
      marginVertical: 10, // Vertical space between each discount container
      marginHorizontal: 10, // Horizontal space from the edges of the screen
      borderRadius: 10, // Rounded corners for each discount container
      // Shadow for depth - Optional, remove if not desired
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    discountTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#58A6FF', // Using the app's highlight color
    },
    discountDescription: {
      fontSize: 16,
      color: '#C9D1D9', // Light grey for text, ensuring readability
      marginTop: 5, // Space between the title and description
    },
    // Existing modal and button styles...
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22,
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center",
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center",
    },
    modalInstruction: {
      marginBottom: 15,
    },
  });

export default PubDiscounts;
