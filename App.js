import React, { useState, useEffect } from 'react';
import AppNavigator from './AppNavigator';
import Login from './Login'; // Ensure you have created this component
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function App() {
  const [userLoggedIn, setUserLoggedIn] = useState(false);

  useEffect(() => {
    // Attempt to fetch a user's login state from storage
    const fetchLoginState = async () => {
      const isLoggedIn = await AsyncStorage.getItem('isLoggedIn');
      setUserLoggedIn(isLoggedIn === 'true');
    };

    fetchLoginState();
  }, []);

  const handleSignIn = async (userInfo) => {
    // Here you can handle the user info if needed
    setUserLoggedIn(true);
    await AsyncStorage.setItem('isLoggedIn', 'true');
  };

  return userLoggedIn ? <AppNavigator /> : <Login onSignIn={handleSignIn} />;
}
