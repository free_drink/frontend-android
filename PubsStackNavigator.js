import { createStackNavigator } from '@react-navigation/stack';
import PubsList from './PubsList';
import PubDetails from './PubDetails'; // Make sure this import is correct
import PubDiscounts from './PubDiscounts'; 

const Stack = createStackNavigator();

function PubsStackNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="PubsList" component={PubsList} options={{ title: 'Pubs List' }} />
      <Stack.Screen name="PubDetails" component={PubDetails} options={{ title: 'Pub Details' }} />
      <Stack.Screen name="PubDiscounts" component={PubDiscounts} options={{ title: 'Pub Discounts' }} />
    </Stack.Navigator>
  );
}

export default PubsStackNavigator;
