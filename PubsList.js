import React from 'react';
import { View, Text, FlatList, StyleSheet, Image, SafeAreaView, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements'; // Ensure react-native-elements is installed

// Sample data
const pubs = [
  {
    id: '1',
    name: 'Pub One',
    description: 'A great place to relax.',
    image: 'https://lh3.googleusercontent.com/p/AF1QipP5KPQiPtWdFlyI1DuvM7KLXPon5-XaUmBfDnj2=s680-w680-h510',
    town: 'Townsville',
    openingTime: '12:00 PM',
    closingTime: '12:00 AM',
    discounts: [
      { title: 'Happy Hour', description: '50% off all drinks from 5pm to 7pm.', redeemInstructions: 'Show this coupon to the bartender.', type: 'camera' },
      { title: 'Student Night', description: '20% off for students every Thursday.', redeemInstructions: 'Present your student ID at the counter.', type: 'camera' },
      { title: 'Instagram Special', description: 'Publish instastory and get two drinks for the price of one.', redeemInstructions: 'Share an Instastory tagging [@ThePub] and @FreeDrink to enjoy two drinks for the price of one. Don\'t miss out on this exclusive 2-for-1 deal!', type: 'instagram' },
    ],
  },
  {
    id: '2',
    name: 'Pub Two',
    description: 'A cozy spot for drinks.',
    image: 'https://lh3.googleusercontent.com/p/AF1QipP5KPQiPtWdFlyI1DuvM7KLXPon5-XaUmBfDnj2=s680-w680-h510',
    town: 'Cityville',
    openingTime: '4:00 PM',
    closingTime: '2:00 AM',
    discounts: [
      { title: 'Happy Hour', description: '50% off all drinks from 5pm to 7pm.', redeemInstructions: 'Show this coupon to the bartender.', type: 'camera' },
      { title: 'Student Night', description: '20% off for students every Thursday.', redeemInstructions: 'Present your student ID at the counter.', type: 'camera' },
      { title: 'Instagram Special', description: 'Publish instastory and get two drinks for the price of one.', redeemInstructions: 'Share an Instastory tagging [@ThePub] and @FreeDrink to enjoy two drinks for the price of one. Don\'t miss out on this exclusive 2-for-1 deal!', type: 'instagram' },
    ],
  },
  {
    id: '3',
    name: 'Pub Three',
    description: 'A lively pub with live music.',
    image: 'https://lh3.googleusercontent.com/p/AF1QipP5KPQiPtWdFlyI1DuvM7KLXPon5-XaUmBfDnj2=s680-w680-h510',
    town: 'Villageville',
    openingTime: '6:00 PM',
    closingTime: '1:00 AM',
    discounts: [
      { title: 'Happy Hour', description: '50% off all drinks from 5pm to 7pm.', redeemInstructions: 'Show this coupon to the bartender.', type: 'camera' },
      { title: 'Student Night', description: '20% off for students every Thursday.', redeemInstructions: 'Present your student ID at the counter.', type: 'camera' },
      { title: 'Instagram Special', description: 'Publish instastory and get two drinks for the price of one.', redeemInstructions: 'Share an Instastory tagging [@ThePub] and @FreeDrink to enjoy two drinks for the price of one. Don\'t miss out on this exclusive 2-for-1 deal!', type: 'instagram' },
    ],
  }
  // Add similar details for other pubs...
];

function PubsList({ navigation }) { // Ensure navigation is received as a prop
  return (
    <SafeAreaView style={styles.safeArea}>
      <FlatList
        data={pubs}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate('PubDetails', { pub: item })}>
            <Card containerStyle={styles.cardContainer}>
              <Image source={{ uri: item.image }} style={styles.image} />
              <Text style={styles.cardTitle}>{item.name}</Text>
              <Text style={styles.description}>{item.description}</Text>
              <View style={styles.detailsContainer}>
                <Text style={styles.detailsText}><Text style={styles.boldText}>Town:</Text> {item.town}</Text>
                <Text style={styles.detailsText}><Text style={styles.boldText}>Hours:</Text> {item.openingTime} - {item.closingTime}</Text>
              </View>
            </Card>
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#0D1117', // A deep blue/black background
  },
  cardContainer: {
    backgroundColor: '#161B22', // Slightly lighter background for cards
    borderRadius: 10,
    padding: 20,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  cardTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#58A6FF', // ChatGPT-like blue for headings
    marginBottom: 10,
  },
  image: {
    width: '100%',
    height: 200,
    borderRadius: 10,
    marginBottom: 15,
  },
  description: {
    fontSize: 16,
    color: '#C9D1D9', // Light grey for text
    marginBottom: 10,
  },
  detailsContainer: {
    marginTop: 10,
  },
  detailsText: {
    fontSize: 16,
    color: '#C9D1D9', // Keeping the text color consistent
    marginBottom: 5,
  },
  boldText: {
    fontWeight: 'bold',
    color: '#58A6FF', // Using the blue for bold text as well
  },
});
export default PubsList;
